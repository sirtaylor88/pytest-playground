import json
from unittest.mock import patch

from django.core.mail import send_mail


# mailoutbox and settings here are standard pytest fixtures (helpers)
# https://pytest-django.readthedocs.io/en/latest/helpers.html
def test_send_email_should_succeed(mailoutbox, settings) -> None:
    # In memory Backend to use in development environment
    settings.EMAIL_BACKEND = "django.core.mail.backends.locmem.EmailBackend"
    assert len(mailoutbox) == 0

    # Send message
    send_mail(
        subject="Test subject here",
        message="Test here is the message.",
        from_email="test_email@gmail.com",
        recipient_list=["test_email_2@gmail.com"],
        fail_silently=False,
    )

    # Test that one message has been sent
    assert len(mailoutbox) == 1

    # Verify that the subject of the first message is correct
    assert mailoutbox[0].subject == "Test subject here"


def test_send_email_without_ars_should_send_empty_email(client) -> None:
    with patch(
        "api.src.companies.views.send_mail"
    ) as mocked_send_mail_function:
        response = client.post(path="/send-email")
        response_content = json.loads(response.content)
        assert response.status_code == 200
        assert response_content["status"] == "success"
        assert response_content["info"] == "email sent successfully"
        mocked_send_mail_function.assert_called_with(
            subject=None,
            message=None,
            from_email="nhattai.nguyen88@gmail.com",
            recipient_list=["nhattai.nguyen88@gmail.com"],
        )


def test_send_email_with_get_verb_should_fail(client) -> None:
    response = client.get(path="/send-email")
    assert response.status_code == 405
    assert json.loads(response.content) == {
        "detail": 'Method "GET" not allowed.'
    }
